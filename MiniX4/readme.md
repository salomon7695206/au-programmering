Link to program: https://salomon7695206.gitlab.io/au-programmering/MiniX4/index.html
<br>
Link to source: https://gitlab.com/salomon7695206/au-programmering/-/blob/main/MiniX4/sketch.js

# Screenshot
![Screenshot__54_](/uploads/9a92bf9a7a93bc63984340e774551a1b/Screenshot__54_.png)
<br>
![Screenshot__55_](/uploads/d512cc831a14f941713eccc158a64fca/Screenshot__55_.png)
<br>
## Introduction
<br>
Captured
<br>
My product provides with four cameras that depicts four different ways a camera can capture from your build-in camera on your laptop.
On the first caption, there is a filter, this filter makes the way it sees you different from what it normally looks like. In this capture, you can see your shape in colors that has not much vibrant unless there is very vibrant colors involved.  The second the caption looks normal. On the third, you can use a slider to change the saturation from red to blue. Under this capture is a slider. This provides with a colorful variation instead of what you normally would look like. However, it changes the white balance from red to green, to blue. On the fourth, if you press on the image that the camera has captured of you it also makes the saturation different. In this capture, the hue saturation is much less visible than the third capture. 
Under the fourth capture is a text that says, “Press on the image” to indicate there is a change when you press on the image.
Randomly there is also a text that says to press key to hear the truth and if you do there is a text that says you are being monitored. 
<br>
### Syntaxes
In this miniX I have learned how to use the webcam in a code.
```
capture = createCapture(VIDEO);
capture.hide();
```
This with the `let capture` makes it possible to use the build in camera in my code

```
Variable 
Let slider;
In setup
slider = createSlider(0, 255, 127);
slider.position(740,250);
In draw
strokeWeight(2);
stroke(slider.value(), 255, 255);
fill(slider.value(), 255, 255, 127);
rect(640,0,320,240);
```
In this miniX I also have learned how to use a slider. I have made a rectangle slider positioned in the third capture. Where the value changes in this context the value is the colors that changes from 0 to 255 which is in this context red, green and blue. It also lets me position it where I want it, which is under the third capture. 
```
Variable
let r, g, b;
In setup
colorMode(HSB, 255);
In Draw
image(capture,960,0,320,240);
strokeWeight(2);
stroke(r, g, b);
fill(r, g, b, 127);
rect(960,0,320,240);
In mousePressed() 
let d = dist(mouseX, mouseY, 960,0);
if (d < 260) {
r = random(255);
g = random(255);
b = random(255);
```
I have also learned how to change with help by if statement how to change color on a rectangle if the mouse is pressed. I have positioned the rectangle in the fourth capture. Where I can change the hue saturation. To do that I made variables r, g, b in order to change the hue saturation I made a stroke and fill with r, g, b in order to change the color when the mouse is clicked. I made an if-statement to check if the mouse is in the rectangle via a variable in the if statement. And to add pick a new color when the mouse is clicked. 
<br>
#### Reflection
What I wanted to do with my miniX was to use the camera and the mouse to show that without really knowing we give many data away. For my miniX to work it needs data from the camera but also from the mouse. In a world where we in daily life use technologies that collects thousands and thousands of data, we aren’t really taught what these data’s are used for, or not taught where we can figure out what they’re used for and how. My miniX is a little example of how much data is collected. wWe can see that a little thing as my minix can collect data about if the webcam is turned on or not. Also if the key or mouse is clicked and where the mouse is. In addition if a thing such as a slider is being moved. It can also collect if my mouse is in a certain place that can make a little rectangle change color if I press my mouse inside the rectangle. These data’s can be used to read behaviors that can be used by companies that can use them for marketing. It does not take much to collect data, and most of the times we don’t even realize we are giving data away. 
