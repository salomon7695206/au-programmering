Link to program: https://salomon7695206.gitlab.io/au-programmering/MiniX3/index.html
Link to source: https://gitlab.com/salomon7695206/au-programmering/-/blob/main/MiniX3/index.html

# What I made
<br>
What I have produced is a very simple throbber. The throbber is an ellipse with different colors. On the screen it says wait a moment. 
If you press the mouse the wait a moment text becomes impatient much?
However if you press any key there is a text added that says still loading…
I have done that because I wanted to make a throbber that I think is pleasing to look at. 
<br>
<br>
## Syntaxes
<br>
The syntaxt I have used is pretty generic. I have copied the syntax one by one from the book to learn what the different syntaxes do in order to understand them. I did that cause I didn't have an understanding of how the different syntaxes work. 
<br>
My loop is this
<br>

```
let num = 12;
 translate(width/2, height/2);
 let cir = 360/num*(frameCount%num);
 rotate(radians(cir));
 noStroke();
 fill(0,191,255);
 ellipse(100,10,200,22);
```

<br>
As you can see it's the same as the one in the book.
The first line is the one that determines how many ellipses there are in the throbber.
The second line determines where the throbber is on the screen.
The third one makes it possible for it to go round and round. If I were to change the number to 180 it would only go in half circles.
The fourth line makes the ellipses rotate function.
Nostroke makes it there is no outline in the ellipse.
The ellipse makes an oval.
<br>
<br>

```
if (mouseIsPressed === true) {
fill(25,25,112);
stroke(255);
textSize(80)
text('Impatient much?',350,330);
} else {
fill(135,206,250);
noStroke();
textSize(20);
text('Wait a moment', 550,550)
```

Here I have made an `if statement`. So it's basically determines that if you press the mouse it makes a text that says impatient much rather that its else statement which is a text that says wait a moment. 
<br>

```
if (keyIsPressed === true) {
fill(25,25,112);
stroke(255);
textSize(80);
text('Still loading...',420,330);
}
```

<br>
<br>
I also made an if statement where if you press any key there is a text coming up on the screen. 

### Reflection
<br>
The reason I have made this throbber is because I wanted to make a throbber for me. A thobber I find pleasing to look at. The reason for that is if I want to wait I want to have pleasing throbbers to look at to be captivated so the time feels like it goes by faster even though it's not. I feel more impatient with the throbbers that companies use. So I wanted to make something that is more entertaining to look at while not changing too much so you don't know what is going on.

I also made some text for those who is more impatient than I am. Trying to tease them a bit. The reason is just pettyness from my side. I find it funny and YOLO so why not make it fun.
<br>

