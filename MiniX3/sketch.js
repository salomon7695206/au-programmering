function setup() {
  //create a drawing canvas
  createCanvas(windowWidth, windowHeight);
  frameRate (12);
 }

 function draw() {


  if (mouseIsPressed === true) {
    fill(25,25,112);
    stroke(255);
    textSize(80);
    text('Impatient much?',width/2, height/2);
  } else {
    fill(135,206,250);
    noStroke();
    textSize(20);
    textAlign(CENTER);
    text('Wait a moment', width/2, height/2);   

    if (keyIsPressed === true) {
    fill(25,25,112);
    stroke(255);
    textSize(80);
    textAligh(CENTER);
    text('Still loading...',width/2, height/2);
    } else {
      fill(135,206,250);
    noStroke();
    textSize(20);
    textAlign(CENTER);
    text('Wait a moment', width/2, height/2);
    }
  background(70,150);
  drawElements();
  
  }
}

 //throbber
 function drawElements() {
   let num = 12;
   translate(width/2, height/2);
   let cir = 360/num*(frameCount%num);
   rotate(radians(cir));
   noStroke();
   fill(0,191,255);
   ellipse(100,10,200,22);

   fill(255,0,0);
   ellipse(80,10,150,22);

   fill(255,20,147);
   ellipse(60,10,100,22);

   fill(0,255,0);
   ellipse(40,10,50,22);

   fill(0,191,255);
   ellipse(20,10,50,22);

   
 }
 function windowResized() {
   resizeCanvas(windowWidth, windowHeight);
  
  }
