# MiniX2
Link to program: https://salomon7695206.gitlab.io/au-programmering/MiniX2/index.html
<br>
Link to source: https://gitlab.com/salomon7695206/au-programmering/-/blob/main/MiniX2/sketch.js

## Introduction
What I wanted to do with my miniX was to make awareness of prejudice and stereotypes that I encounter as a kalaaleq in Denmark. I encounter them in daily life. I made 6 smileys in total. 3 smileys about the most common prejudice and stereotypes that I encounter. The rest is how I see kalaallit as a people. For me to have representation that is more accurate is important. Often times when I encounter prejudice and stereotypes it is a picture of my country and people that is wildly outdated.

My people were colonized and with that, there are big consequences. Often times these consequences are exaggerated by the danish media. I know that we have many social problems and I am not denying we have social problems, and they need to be expressed and we need to do a lot to do something about the problems, but they a little exaggerated. What I want with my MiniX is to have a more accurate representation. With representation that is more accurate, we reclaim our Identity. With reclaiming, our identity comes healing as people as well as individuals. Healing brings us to be able to stop the vicious cycle that is continuously going on. We can start in something as little as an accurate emoji to reclaim our Identity and towards healing. 

### The different syntaxes
The different syntaxes I have used are simple. 
<br>
fill(135,206,250);
<br>
arc(115,210,160,120,-PI,0,CHORD);
<br>
fill(255);
<br>
ellipse(115,114,140,140);
<br>
fill(222,184,135);
<br>
ellipse(115,114,110,110);

Here I have used fill to fill the face with a skincolor that matches nearly mine. I used an arc to make the shoulders and ellipse to make a round head. 
<br>
<br>
curve(130,95,120,125,110,125,100,100);
<br>
curve(130,95,135,140,95,135,100,100);

I had really a hard time trying to figure out the curve syntax. For the life of me I couldn’t figure out how to make it curve the way I wanted it, but in the end I found out, that the last two decides if it curves up or down in the right end, and the first two decides if the left one curves up or down. This curve code I still am not sure if I understand it or it was out of pure luck that they got in the right places. 
<br>
<br>
<br>
textSize(20);
<br>
fill(1);
<br>
text('Colonial mentality',230,310)

The text syntax was the most easy syntax I have use. I had not used it before but I had no problem understanding it or executing it. I love that!!
<br>
<br>
<br>
if (mouseIsPressed === true) {
<br>
} else {
<br>
fill(255);
<br>
stroke(1);
<br>
square(820,20,190);
<br>
square(1010,20,190);
<br>
square(1010,210,190);
<br>
square(820,210,190);

For the first time I used an if-statement and it was if mouseIsPressed and used about an hour trying to understand it and what to put in and where. In a way I used it I wanted to have white squares and I want it to pop up when the mouse or key is pressed. In a way to suspend excitement. So when you press the mouse the smileys are revealed. I was mildly impressed that this worked for me in the end. 

#### Reflection
I used a lot of time to make the smileys. And I was actually enjoying it. I wanted to express the importance of accurate representation in my miniX. I am really happy about the result. 
There was a time where I did the Igloo I was wildly frustrated because the curves did not want to cooperate with me. Beyond that I have not experienced frustration like the miniX before. 
